from typing import Union

from starlette.middleware.cors import CORSMiddleware
# a rentrer dans le terminal, puis ouvrir le lien du localhost
"uvicorn main:app --reload"

from fastapi import FastAPI
from pydantic.main import BaseModel

app = FastAPI()
#donne l'acces au localhost ou à tout *
origins = [
    "http://127.0.0.1:5500",
    "*"
]
#autre couche de secutité plus personnalisé
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

#schema = modele de donnée
class Chasseur(BaseModel):
    nom: str
    age: int
    id: int

class ChasseurSansID(BaseModel):
    nom: str
    age: int

#tableau vide dans lequelle on enverra les chasseurs
chasseurs=[]

#creer la route vers localhost/chasseur
@app.post('/chasseur')
#permet d'ajouter des nouveau chasseurs en utilisant le modele ChasseurSansID
def add_chasseur(nouveau_chasseur: ChasseurSansID):
    #on creer un chasseur_normal qui egal le schema Chasseur
    chasseur_normal = Chasseur(
        #nom = nom du nouveau chasseur
        nom = nouveau_chasseur.nom,
        age = nouveau_chasseur.age,
        #permet de mettre un id sur la taille du tableau chasseurs
        id = len(chasseurs)
    )
    # envoi chassuer_normal dans le tableau chasseurs
    chasseurs.append(chasseur_normal)
    #affiche chasseur_normal (donc chasseurs avec nom age et id)
    return chasseur_normal


@app.get("/chasseurs")
#recupere la liste des chasseurs
def get_all_chasseurs():
    #renvoi la liste des chasseurs dans le tableau
    return chasseurs



#@app.get('/chasseurs/{id_chasseur}')
#def get_chasseurs_by_id(id_chasseur: int):
#return list(filter(lambda c: c.id == id_chasseur, chasseurs))[0]